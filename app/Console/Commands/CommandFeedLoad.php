<?php

namespace App\Console\Commands;

use App\Repositories\LoadRepository;
use Illuminate\Console\Command;

class CommandFeedLoad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load products from feeds to Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $offers = LoadRepository::getFeedRecordsToLoad();
        if (!count($offers)) return;
        foreach ($offers as $offer) {
            \DB::transaction(function () use ($offer) {
                if ((strlen($offer->ISBN) > 1) || $offer->author || $offer->page_extent || $offer->year) {
                    $offer->year = LoadRepository::checkYear($offer->year);
                    $offer->page_extent = LoadRepository::checkPageExtent($offer->page_extent);
                    $offer->product_id = LoadRepository::loadProductISBN($offer);
                    LoadRepository::insertOffer($offer);
                    LoadRepository::insertPicture($offer);
                    //LoadRepository::insertDescription($offer);
                    //LoadRepository::insertAuthor($offer);
                }
                LoadRepository::deleteFeed($offer->primary_id);
            });
        }
    }

}
