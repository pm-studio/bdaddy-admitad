<?php

namespace App\Console\Commands;

use App\Repositories\LoadRepository;
use Foolz\SphinxQL\Drivers\Pdo\Connection;
use Foolz\SphinxQL\SphinxQL;
use Illuminate\Console\Command;

class CommandFeedLoadNullISBN extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:loadnullisbn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load products from feeds to Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $conn = new Connection();
        $conn->setParams([
            'host' => env('SPHINX_HOST'),
            'port' => env('SPHINX_PORT'),
        ]);

        $offers = LoadRepository::getFeedRecordsToLoadNullISBN();
        if (!count($offers)) return;
        foreach ($offers as $offer) {
            \DB::transaction(function () use ($offer, $conn) {
                if ((strlen($offer->ISBN) > 1) || $offer->author || $offer->page_extent || $offer->year) {
                    $offer->year = LoadRepository::checkYear($offer->year);
                    $offer->page_extent = LoadRepository::checkPageExtent($offer->page_extent);

                    $result = (new SphinxQL($conn))
                        ->select('id')
                        ->from('BookdaddyProductsIndex')
                        ->where('author', '=', $offer->author)
                        ->where('name', '=', $offer->name)
                        ->execute()
                        ->fetchAssoc();

                    if ($result) {
                        $offer->product_id = intval($result['id']);
                    } else {
                        $offer->product_id = LoadRepository::insertProduct($offer);
                        LoadRepository::insertPicture($offer);
                    }
                    LoadRepository::insertOffer($offer);
                    //LoadRepository::insertDescription($offer);
                    //LoadRepository::insertAuthor($offer);
                }
                LoadRepository::deleteFeed($offer->primary_id);
            });
        }
    }

}
