<?php

namespace App\Console\Commands;

use App\Repositories\DeletedRepository;
use Illuminate\Console\Command;

class CommandFeedDeleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:deleted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deleted products from feeds to Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $offers = DeletedRepository::getFeedRecordsToDelete();
        if (!count($offers)) return;
        foreach ($offers as $offer) {
            \DB::transaction(function () use ($offer) {
                DeletedRepository::updateOffer($offer);
                DeletedRepository::deleteFeed($offer->primary_id);
            });
        }
    }
}
