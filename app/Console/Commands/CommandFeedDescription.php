<?php

namespace App\Console\Commands;

use App\Repositories\DescriptionRepository;
use Illuminate\Console\Command;
use Throwable;

class CommandFeedDescription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:description';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load descriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Throwable
     */
    public function handle()
    {
        $descriptions = DescriptionRepository::getDescriptionRecordsToLoad();
        if (!count($descriptions)) return;
        \DB::transaction(function () use ($descriptions) {
            foreach ($descriptions as $description) {
                $product = DescriptionRepository::getProductByID($description->product_id);
                if (strlen($description->description) > strlen($product->description)) {
                    DescriptionRepository::updateProductDescription($product->id, $description->description);
                }
                DescriptionRepository::deleteDescription($description->id);
            }
        });
    }

}
