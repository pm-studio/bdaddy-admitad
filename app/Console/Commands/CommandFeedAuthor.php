<?php

namespace App\Console\Commands;

use App\Repositories\AuthorRepository;
use Illuminate\Console\Command;
use Throwable;

class CommandFeedAuthor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:author';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load authors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Throwable
     */
    public function handle()
    {
        $authors = AuthorRepository::getAuthorRecordsToLoad();
        if (!count($authors)) return;
        \DB::transaction(function () use ($authors) {
            foreach ($authors as $author) {
                $product = AuthorRepository::getProductByID($author->product_id);
                if (strlen($author->author) > strlen($product->author)) {
                    AuthorRepository::updateProductAuthor($product->id, $author->author);
                }
                AuthorRepository::deleteAuthor($author->id);
            }
        });
    }

}
