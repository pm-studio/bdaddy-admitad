<?php

namespace App\Console\Commands;

use App\Repositories\FeedRepository;
use Illuminate\Console\Command;

class CommandFeedInsert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert feeds from files';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!\Storage::disk('local')->allFiles('feeds')) return;
        $advcampaign_id = FeedRepository::getFeedIDToLoad();
        if (!FeedRepository::checkFilesToLoad($advcampaign_id)) return;
        FeedRepository::feedTransaction($advcampaign_id);
    }
}
