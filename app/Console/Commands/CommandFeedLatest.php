<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Throwable;

class CommandFeedLatest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:latest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Throwable
     */
    public function handle()
    {
        $product = \DB::table('products')
            ->whereNotNull(['products.author', 'products.name', 'isbns.isbn'])
            ->where('products.author', '!=', '')
            ->leftJoin('pictures', 'products.id', '=', 'pictures.product_id')
            ->leftJoin('advcampaigns', 'pictures.advcampaign_id', '=', 'advcampaigns.id')
            ->leftJoin('isbns', 'products.id', '=', 'isbns.product_id')
            ->orderByDesc('products.id')
            ->orderByDesc('advcampaigns.picture_priority')
            ->limit(10)
            ->get([
                'products.id as id',
                'products.author as author',
                'products.name as name',
                'pictures.url as image',
            ])
            ->toArray();
        file_put_contents('/home/p/pavelmy9/api.bookpa.ru/public_html/storage/app/latest.json', json_encode($product, JSON_HEX_TAG));
        file_put_contents('/home/p/pavelmy9/api.bookdaddy.ru/public_html/storage/app/latest.json', json_encode($product, JSON_HEX_TAG));
        return;
    }

}
