<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Throwable;

class CommandSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws Throwable
     */
    public function handle()
    {
        $limit = 50000;
        $count = \DB::table('products')->count();
        $now = date("Y-m-d") . "T" . date("H:i:s");
        $cycle = intval(ceil($count/$limit));
        $urls = '';
        $sitemaps = '';

        /*books.xml*/
        for ($i = 1; $i <= $cycle; $i++) {
            $offset = $limit * ($i - 1);
            $limitRecord = ($i == $cycle) ? $count - (($cycle - 1) * $limit) : $limit;
            $products = \DB::table('products')
                ->limit($limitRecord)
                ->offset($offset)
                ->get(['id', 'updated_at'])
                ->toArray();
            foreach ($products as $product) {
                $updated_at = strtotime($product->updated_at);
                $urls .= "<url><loc>https://bookdaddy.ru/book/$product->id</loc><lastmod>"
                    . date("Y-m-d", $updated_at)
                    . "T"
                    . date("H:i:s", $updated_at)
                    . "</lastmod></url>";
            }
            $urlsCount = $i * $limit;
            $sitemaps .= '    <sitemap>
        <loc>https://bookdaddy.ru/books-' . $urlsCount . '.xml</loc>
        <lastmod>' . $now . '</lastmod>
    </sitemap>' . PHP_EOL;
            $sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">' . $urls . '</urlset>';
            file_put_contents("/home/p/pavelmy9/bookdaddy.ru/static/books-$urlsCount.xml", $sitemap);
            $urls = '';
        }

        /*pages.xml*/
        $pages = ['', 'search'];
        foreach ($pages as $page) $urls .= "<url><loc>https://bookdaddy.ru/$page</loc><lastmod>$now</lastmod></url>";
        $sitemap = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">' . $urls . '</urlset>';
        file_put_contents("/home/p/pavelmy9/bookdaddy.ru/static/pages.xml", $sitemap);

        /*sitemap.xml*/
        $sitemap = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>https://bookdaddy.ru/pages.xml</loc>
        <lastmod>' . $now . '</lastmod>
    </sitemap>
    ' . $sitemaps . '
</sitemapindex>';
        file_put_contents("/home/p/pavelmy9/bookdaddy.ru/static/sitemap.xml", $sitemap);
        return;
    }

}
