<?php

namespace App\Console\Commands;

use App\Repositories\AdvcampaignRepository;
use Illuminate\Console\Command;

class CommandFeedDownload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download new Feed';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $advcampaign = AdvcampaignRepository::getAdvcampaignToImport();
        if ($advcampaign)
        {
            AdvcampaignRepository::updateImportTime($advcampaign->id);
            system(AdvcampaignRepository::getDownloadCMD($advcampaign));
        }
    }
}
