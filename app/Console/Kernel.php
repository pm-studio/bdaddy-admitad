<?php

namespace App\Console;

use App\Console\Commands\CommandFeedAuthor;
use App\Console\Commands\CommandFeedDeleted;
use App\Console\Commands\CommandFeedDescription;
use App\Console\Commands\CommandFeedDownload;
use App\Console\Commands\CommandFeedInsert;
use App\Console\Commands\CommandFeedLatest;
use App\Console\Commands\CommandFeedLoad;
use App\Console\Commands\CommandFeedLoadNullISBN;
use App\Console\Commands\CommandFeedPopular;
use App\Console\Commands\CommandSitemap;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CommandFeedDownload::class,
        CommandFeedInsert::class,
        CommandFeedLoad::class,
        CommandFeedDeleted::class,
        CommandFeedDescription::class,
        CommandFeedAuthor::class,
        CommandFeedLatest::class,
        CommandFeedPopular::class,
        CommandFeedLoadNullISBN::class,
        CommandSitemap::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('feed:download')
            ->everyFifteenMinutes()
            ->between('1:00', '2:00')
            ->withoutOverlapping();
        $schedule->command('feed:insert')
            ->everyFifteenMinutes()
            ->between('1:15', '2:15')
            ->withoutOverlapping();
        $schedule->command('feed:load')
            ->everyMinute()
            ->between('2:20', '3:00')
            ->withoutOverlapping();
        $schedule->command('feed:deleted')
            ->everyMinute()
            ->between('2:20', '3:00')
            ->withoutOverlapping();
        /*$schedule->command('feed:description')
            ->everyMinute()
            ->between('3:20', '3:40')
            ->withoutOverlapping();
        $schedule->command('feed:author')
            ->everyMinute()
            ->between('3:40', '4:00')
            ->withoutOverlapping();*/
        $schedule->command('feed:loadnullisbn')
            ->everyMinute()
            ->between('3:10', '4:00')
            ->withoutOverlapping();
        $schedule->command('feed:popular')
            ->dailyAt('4:01')
            ->withoutOverlapping();
        $schedule->command('feed:latest')
            ->dailyAt('4:02')
            ->withoutOverlapping();
        $schedule->command('sitemap')
            ->dailyAt('4:05')
            ->withoutOverlapping();
    }
}
