<?php

namespace App\Repositories;

/**
 * Class LoadRepository
 * @package App\Repositories
 */
class LoadRepository
{
    static function getFeedRecordsToLoad($limit = 20000)
    {
        return \DB::table("feeds")
            ->whereRaw('CHAR_LENGTH(ISBN) > 1')
            ->whereNull('deleted')
            ->orWhere('deleted','<',1)
            ->orderBy('primary_id')
            ->limit($limit)
            ->get(['primary_id', 'advcampaign_id', 'ISBN', 'author', 'available', 'categoryId', 'id', 'modified_time',
                'description', 'name', 'page_extent', 'picture', 'price', 'url', 'year', 'downloadable', 'format'])
            ->toArray();
    }

    static function getFeedRecordsToLoadNullISBN($limit = 10000)
    {
        return \DB::table("feeds")
            ->whereRaw('CHAR_LENGTH(ISBN) < 2')
            ->whereNull('deleted')
            ->orWhere('deleted','<',1)
            ->orderBy('primary_id')
            ->limit($limit)
            ->get(['primary_id', 'advcampaign_id', 'ISBN', 'author', 'available', 'categoryId', 'id', 'modified_time',
                'description', 'name', 'page_extent', 'picture', 'price', 'url', 'year', 'downloadable', 'format'])
            ->toArray();
    }

    static function checkYear($year)
    {
        $year = substr($year, 0, 4);
        if(!preg_match("(^17[0-9]{2}$|^18[0-9]{2}$|^19[0-9]{2}$|^20[0-9]{2}$)", $year)) $year = null;
        return $year;
    }

    static function checkPageExtent($page_extent)
    {
        $page_extent = intval($page_extent);
        if($page_extent > 10000 || $page_extent == 0) $page_extent = null;
        return $page_extent;
    }

    static function loadProductISBN($offer)
    {
        $product_id = null;
        if ($offer->ISBN) {
            $isbns = explode(",", $offer->ISBN);
            foreach ($isbns as $isbn) {
                $isbn_full = trim($isbn);
                $isbn = str_replace("-", "", $isbn_full);
                if (is_null($product_id)) {
                    $product = self::getProductByISBN($isbn);
                    if ($product) {
                        $product_id = $product->product_id;
                        $product = self::getProductByID($product_id);
                        if(!$product->year && $offer->year) $params['year'] = $offer->year;
                        if(!$product->page_extent && $offer->page_extent) $params['page_extent'] = $offer->page_extent;
                        if(isset($params)) self::updateProduct($product_id, $params);
                    } else {
                        $product_id = self::insertProduct($offer);
                    }
                }
                self::insertISBN($isbn, $isbn_full, $product_id);
            }
        } else {
            // $product = self::getProductByAuthorName($offer->author, $offer->name);
            // $product_id = ($product) ? $product->id : self::insertProduct($offer);
            $product_id = self::insertProduct($offer);
        }
        return $product_id;
    }

    /*static function getProductByAuthorName($author, $name)
    {
        return \DB::table("products")
            ->where("author", "=", $author)
            ->where("name", "=", $name)
            ->first("id");
    }*/

    static function getProductByISBN($isbn)
    {
        return \DB::table("isbns")
            ->where("isbn", "=", $isbn)
            ->first("product_id");
    }

    static function getProductByID($product_id)
    {
        return \DB::table("products")
            ->where("id", "=", $product_id)
            ->first(['page_extent', 'year']);
    }

    static function updateProduct($product_id, $params)
    {
        if(isset($params))
            $params["updated_at"] = date("Y-m-d H:i:s");
            \DB::table("products")
                ->where("id", "=", $product_id)
                ->update($params);
    }

    static function insertISBN($isbn, $isbn_full, $product_id)
    {
        \DB::table("isbns")
            ->insertOrIgnore([
                "isbn" => $isbn,
                "isbn_full" => $isbn_full,
                "product_id" => $product_id,
            ]);
    }

    static function insertProduct($offer)
    {
        return \DB::table("products")
            ->insertGetId([
                "name" => $offer->name,
                "author" => $offer->author,
                "description" => $offer->description,
                "page_extent" => intval($offer->page_extent),
                "year" => intval($offer->year),
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
            ]);
    }

    static function insertOffer($offer)
    {
        \DB::table("offers")
            ->updateOrInsert([
                "offerproduct_id" => $offer->id,
                "advcampaign_id" => $offer->advcampaign_id,
            ],
            [
                "product_id" => $offer->product_id,
                "available" => $offer->available,
                "categoryId" => $offer->categoryId,
                "url" => $offer->url,
                "price" => ($offer->price) ? $offer->price : 0,
                "modified_time" => $offer->modified_time,
                "downloadable" => $offer->downloadable,
                "format" => $offer->format,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
            ]);
    }

    static function insertPicture($offer)
    {
        if ($offer->picture)
            \DB::table("pictures")
                ->updateOrInsert([
                    "product_id" => $offer->product_id,
                    "advcampaign_id" => $offer->advcampaign_id,
                ],
                [
                    "url" => $offer->picture,
                ]);
    }

    static function insertDescription($offer)
    {
        if ($offer->description)
            \DB::table("descriptions")
                ->updateOrInsert([
                    "product_id" => $offer->product_id,
                    "advcampaign_id" => $offer->advcampaign_id,
                ],
                [
                    "description" => $offer->description,
                ]);
    }

    static function insertAuthor($offer)
    {
        if ($offer->author)
        \DB::table("authors")
            ->updateOrInsert([
                "product_id" => $offer->product_id,
                "advcampaign_id" => $offer->advcampaign_id,
            ],
            [
                "author" => $offer->author,
            ]);
    }

    static function deleteFeed(int $id)
    {
        \DB::table("feeds")
            ->where("primary_id", "=", $id)
            ->delete();
    }

}
