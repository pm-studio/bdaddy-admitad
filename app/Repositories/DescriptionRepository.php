<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class DescriptionRepository
 * @package App\Repositories
 */
class DescriptionRepository
{
    /**
     * @param int $limit
     * @return array
     */
    static function getDescriptionRecordsToLoad($limit = 100000)
    {
        return \DB::table("descriptions")
            ->orderBy('id')
            ->limit($limit)
            ->get(['id', 'product_id', 'advcampaign_id', 'description'])
            ->toArray();
    }

    /**
     * @param $product_id
     * @return Model|Builder|object|null
     */
    static function getProductByID(int $product_id)
    {
        return \DB::table("products")
            ->where("id", "=", $product_id)
            ->first(['id', 'description']);
    }

    /**
     * @param $product_id
     * @param $description
     */
    static function updateProductDescription(int $product_id, $description): void
    {
        \DB::table("products")
            ->where("id", "=", $product_id)
            ->update([
                'description' => $description
            ]);
    }

    /**
     * @param int $id
     */
    static function deleteDescription(int $id): void
    {
        \DB::table("descriptions")
            ->where("id", "=", $id)
            ->delete();
    }

}
