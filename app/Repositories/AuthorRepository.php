<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class AuthorRepository
 * @package App\Repositories
 */
class AuthorRepository
{
    /**
     * @param int $limit
     * @return array
     */
    static function getAuthorRecordsToLoad($limit = 100000)
    {
        return \DB::table("authors")
            ->orderBy('id')
            ->limit($limit)
            ->get(['id', 'product_id', 'advcampaign_id', 'author'])
            ->toArray();
    }

    /**
     * @param $product_id
     * @return Model|Builder|object|null
     */
    static function getProductByID(int $product_id)
    {
        return \DB::table("products")
            ->where("id", "=", $product_id)
            ->first(['id', 'author']);
    }

    /**
     * @param $product_id
     * @param $author
     */
    static function updateProductAuthor(int $product_id, $author): void
    {
        \DB::table("products")
            ->where("id", "=", $product_id)
            ->update([
                'author' => $author
            ]);
    }

    /**
     * @param int $id
     */
    static function deleteAuthor(int $id): void
    {
        \DB::table("authors")
            ->where("id", "=", $id)
            ->delete();
    }

}
