<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use stdClass;

/**
 * Class AdvcampaignRepository
 * @package App\Repositories
 */
class AdvcampaignRepository
{
    /**
     * Table
     */
    const TABLE_ADVCAMPAIGNS = "advcampaigns";

    /**
     * @param int $timestamp
     * @return false|string
     */
    static function getLastImport(int $timestamp)
    {
        return ($timestamp) ? date('Y.m.d.H.i', $timestamp) : '';
    }

    /**
     * @param int $uploadPeriod
     * @return Model|Builder|object|null
     */
    static function getAdvcampaignToImport(int $uploadPeriod = 20)
    {
        $advcampaign = \DB::table(self::TABLE_ADVCAMPAIGNS)
            ->where([
                ['importable', '=', 1],
                ['import_time', '=', 0],
                ['last_import', '<', time() - $uploadPeriod * 3600],
            ])
            ->orderBy('import_priority')
            ->first(['id', 'last_import', 'template_csv_id', 'template_xml_id']);

        if ($advcampaign) $advcampaign->last_import = self::getLastImport($advcampaign->last_import);
        return $advcampaign;
    }

    /**
     * @param int $id
     * @return int
     */
    static function updateImportTime(int $id)
    {
        return \DB::table(self::TABLE_ADVCAMPAIGNS)
            ->where('id', '=', $id)
            ->update([
                'import_time' => time(),
            ]);
    }

    /**
     * @param stdClass $advcampaign
     * @param string $feed_type
     * @return false|string
     */
    static function getFeedUrl(stdClass $advcampaign, $feed_type)
    {
        if ($feed_type == "csv") $feed_type = $advcampaign->template_csv_id;
        if ($feed_type == "xml") $feed_type = $advcampaign->template_xml_id;
        return "http://export.admitad.com/kz/webmaster/websites/"
            . env("ADMITAD_WEBSITE_ID")
            . "/products/export_adv_products/?"
            . http_build_query([
                'currency' => env('ADMITAD_CURRENCY'),
                'code' => env('ADMITAD_CODE'),
                'user' => env('ADMITAD_USER'),
                'template' => $feed_type,
                'feed_id' => $advcampaign->id,
                'last_import' => $advcampaign->last_import,
            ]);
    }

    /**
     * @param stdClass $advcampaign
     * @return string
     */
    static function getDownloadCMD(stdClass $advcampaign)
    {
        $feedCSV = self::getFeedUrl($advcampaign, "csv");
        $feedXML = self::getFeedUrl($advcampaign, "xml");
        $path = '/home/p/pavelmy9/bookdaddy-admitad/storage/app/feeds/';

        return sprintf(
            'wget -O %s "%s"
                wget -O %s "%s"
                ',
            "$path$advcampaign->id.csv", $feedCSV,
            "$path$advcampaign->id.xml", $feedXML
        );
    }

}
