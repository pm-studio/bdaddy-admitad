<?php

namespace App\Repositories;

/**
 * Class DeletedRepository
 * @package App\Repositories
 */
class DeletedRepository
{
    static function getFeedRecordsToDelete($limit = 20000)
    {
        return \DB::table("feeds")
            ->where('deleted','=',1)
            ->orderBy('primary_id')
            ->limit($limit)
            ->get(['primary_id', 'advcampaign_id', 'id'])
            ->toArray();
    }

    static function updateOffer($offer)
    {
        \DB::table("offers")
            ->where([
                ["offerproduct_id", "=", $offer->id],
                ["advcampaign_id", "=", $offer->advcampaign_id]
            ])
            ->update([
                "deleted" => 1,
            ]);
    }

    static function deleteFeed(int $id)
    {
        \DB::table("feeds")
            ->where("primary_id", "=", $id)
            ->delete();
    }

}
