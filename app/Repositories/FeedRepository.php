<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class FeedRepository
 * @package App\Repositories
 */
class FeedRepository
{
    /**
     * Table
     */
    const TABLE_ADVCAMPAIGNS = "advcampaigns";

    /**
     * @param $csv
     * @param int $advcampaign_id
     * @return bool
     */
    static function loadCSVFeed($csv, int $advcampaign_id)
    {
        $sql = "";
        $first_sql = "";
        $i = 0;
        $k = 1;
        $index = [];

        while (($line = fgetcsv($csv, 0, ";")) !== false)
        {
            if (!$sql) {
                $first_sql = "(advcampaign_id," . implode(",", $line) . ") VALUES ";
                foreach ($line as $key => $value) {
                    $index[$value] = $key;
                }
                $sql .= $first_sql;
            } else {
                if(isset($index["deleted"])) $line[$index["deleted"]] = ($line[$index["deleted"]] == "true") ? 1 : 0;
                if(isset($index["downloadable"])) $line[$index["downloadable"]] = ($line[$index["downloadable"]] == "true") ? 1 : 0;
                $sql .= ("($advcampaign_id,'"
                    . implode("','", str_replace("'", "''", str_replace("\\", "", $line)))
                    . "'),");
            }
            $i++;
            if ($i > $k * 10000) {
                $sql = substr($sql, 0, -1);
                \DB::insert("INSERT INTO feeds $sql;");
                $sql = $first_sql;
                $k++;
            }
        }
        fclose($csv);
        if($sql == $first_sql) return false;
        $sql = substr($sql, 0, -1);
        \DB::insert("INSERT INTO feeds $sql;");
        return true;
    }

    /**
     * @param $xml
     * @param int $advcampaign_id
     */
    static function loadXMLFeed($xml, int $advcampaign_id)
    {
        foreach ($xml->shop->offers->offer as $offer) {
            \DB::table("feeds")
                ->where([
                    ['advcampaign_id', '=', $advcampaign_id],
                    ['id', '=', $offer['id']],
                ])
                ->update([
                    "available" => (strval($offer["available"]) == "true") ? 1 : 0,
                    "categoryId" => intval($offer->categoryId),
                    "description" => trim($offer->description),
                ]);
        }
    }

    /**
     * @param int $id
     * * @return int
     */
    static function updateLastImport(int $id): void
    {
        \DB::update("UPDATE advcampaigns SET last_import=import_time, import_time=0 WHERE id=$id;");
    }

    /**
     * @param int $id
     * * @return int
     */
    static function resetLastImport(int $id): void
    {
        \DB::update("UPDATE advcampaigns SET import_time=0 WHERE id=$id;");
    }

    /**
     * @param $feed
     */
    static function deleteFeedFiles(int $advcampaign_id): void
    {
        \Storage::disk('local')->delete('feeds' . DIRECTORY_SEPARATOR . $advcampaign_id . '.csv');
        \Storage::disk('local')->delete('feeds' . DIRECTORY_SEPARATOR . $advcampaign_id . '.xml');
    }

    /**
     * @return int
     */
    static function getFeedIDToLoad(): int
    {
        $advcampaign = \DB::table(self::TABLE_ADVCAMPAIGNS)
            ->where([
                ['importable', '=', 1],
                ['import_time', '!=', 0],
            ])
            ->orderBy('import_priority')
            ->limit(1)
            ->first(['id']);

        return ($advcampaign) ? $advcampaign->id : 0;
    }

    /**
     * @param int $advcampaign_id
     * @return bool
     */
    static function checkFilesToLoad(int $advcampaign_id): bool
    {
        $path_csv = storage_path(implode(DIRECTORY_SEPARATOR, ["app", "feeds", $advcampaign_id . ".csv"]));
        if (!file_exists($path_csv)) return false;
        if (time() - filemtime($path_csv) < 10) return false;

        $path_xml = storage_path(implode(DIRECTORY_SEPARATOR, ["app", "feeds", $advcampaign_id . ".xml"]));
        if (!file_exists($path_xml)) return false;
        if (time() - filemtime($path_xml) < 10) return false;

        if (!filesize($path_csv) || !filesize($path_xml)) {
            self::resetLastImport($advcampaign_id);
            self::deleteFeedFiles($advcampaign_id);
            return false;
        }

        return true;
    }

    static function feedTransaction(int $advcampaign_id): void
    {
        \DB::transaction(function () use ($advcampaign_id) {
            $path = storage_path(implode(DIRECTORY_SEPARATOR, ["app", "feeds", $advcampaign_id . ".csv"]));
            $csv = fopen($path, "r");
            if (self::loadCSVFeed($csv, $advcampaign_id)) {
                $path = storage_path(implode(DIRECTORY_SEPARATOR, ["app", "feeds", $advcampaign_id . ".xml"]));
                $xml = simplexml_load_file($path);
                self::loadXMLFeed($xml, $advcampaign_id);
            }
            self::updateLastImport($advcampaign_id);
            self::deleteFeedFiles($advcampaign_id);
        });
    }

}
