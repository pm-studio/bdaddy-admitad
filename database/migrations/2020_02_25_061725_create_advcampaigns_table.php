<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvcampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advcampaigns', function (Blueprint $table) {
            $table->bigIncrements('primary_id');
            $table->unsignedBigInteger('id')->unique();
            $table->text('name');
            $table->unsignedInteger('template_csv_id')->nullable();
            $table->unsignedInteger('template_xml_id')->nullable();
            $table->boolean('importable')->default(1);
            $table->unsignedInteger('import_priority')->default(0);
            $table->unsignedInteger('import_time')->default(0);
            $table->unsignedInteger('last_import')->default(0);
            $table->unsignedInteger('picture_priority')->default(0);
            $table->text('image')->nullable();
            $table->text('status')->nullable();
            $table->longText('raw_description')->nullable();
            $table->text('site_url')->nullable();
            $table->text('gotolink')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advcampaigns');
    }
}
