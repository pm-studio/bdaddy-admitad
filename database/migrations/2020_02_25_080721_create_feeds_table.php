<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->bigIncrements('primary_id');
            $table->unsignedBigInteger('advcampaign_id');
            $table->string('ISBN')->nullable();
            $table->text('author')->nullable();
            $table->boolean('available')->nullable();
            $table->unsignedBigInteger('categoryId')->nullable();
            $table->string('id', 20);
            $table->string('modified_time')->nullable();
            $table->text('description')->nullable();
            $table->text('name')->nullable();
            $table->string('page_extent')->nullable();
            $table->string('picture')->nullable();
            $table->string('price')->nullable();
            $table->text('url')->nullable();
            $table->string('year')->nullable();
            $table->boolean('deleted')->nullable();
            $table->boolean('downloadable')->nullable();
            $table->string('format',50)->nullable();
            $table->unique(['advcampaign_id','id']);
            $table->foreign('advcampaign_id')->references('id')->on('advcampaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}
