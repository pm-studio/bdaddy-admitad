<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('offerproduct_id', 20);
            $table->unsignedBigInteger('advcampaign_id')->default(0);
            $table->unsignedBigInteger('product_id')->default(0);
            $table->unsignedBigInteger('categoryId')->nullable();
            $table->text('url')->nullable();
            $table->float('price')->default(0);
            $table->boolean('available')->nullable();
            $table->unsignedBigInteger('modified_time')->nullable();
            $table->boolean('deleted')->nullable();
            $table->boolean('downloadable')->nullable();
            $table->string('format',50)->nullable();
            $table->timestamps();
            $table->foreign('advcampaign_id')->references('id')->on('advcampaigns');
            $table->foreign('product_id')->references('id')->on('products');
            $table->unique(['offerproduct_id','advcampaign_id']);
            $table->index('offerproduct_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
